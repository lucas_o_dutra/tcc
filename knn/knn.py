import numpy as np        
import pandas as pd  

#define um n
n=5
#arquivo
dates= "/home/lucas/Documentos/tcc/knn/IRIS.csv"

#url
dates2 = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"

# nome das colunas para o dataser
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# ler o dataset
dataset = pd.read_csv(dates2, names=names)  
dataset.head()

# mostra as 5 primeiras colunas
#print(dataset.head())

#pre procesamento separar os dados da classe
X = dataset.iloc[:, :-1].values  
y = dataset.iloc[:, 4].values  

#separar 70% das amostrars para o treinamento e 30% para teste
from sklearn.model_selection import train_test_split  
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.30)  


#dimencionamento dos dados
from sklearn.preprocessing import StandardScaler  
scaler = StandardScaler()  
scaler.fit(X_train)

X_train = scaler.transform(X_train)  
X_test = scaler.transform(X_test)  

#Treina o knn

from sklearn.neighbors import KNeighborsClassifier  
classifier = KNeighborsClassifier(n_neighbors=n)  
classifier.fit(X_train, y_train)


#Preve os valores da base de teste
y_pred = classifier.predict(X_test)

#Analisa os resultados
from sklearn.metrics import classification_report, confusion_matrix  
print(confusion_matrix(y_test, y_pred))  
print(classification_report(y_test, y_pred))  

#mostrars os x , y
for i in range(0,len(y_pred)):
    print(X_test[i])
    print(y_test[i])
    print("\n")
    

#Le os 4 valores e o classifica
sepalLength = float(raw_input("digite sepalLength:"))

sepalWidth = float(raw_input("digite sepalWidth:"))

petalLength = float(raw_input("digite petalLength:"))

petalWidth = float(raw_input("digite petalWidth:"))
    

valores= np.array([[sepalLength,sepalWidth,petalLength,petalWidth]])

valores_scalado= scaler.transform(valores)
resultado = classifier.predict(valores_scalado)
print(resultado)
print(valores_scalado)
 

